import requests, threading, shutil, time, ctypes
from colorama import init, Fore
from bs4 import BeautifulSoup

lock = threading.Lock()
req = requests.Session()
init()

ctypes.windll.kernel32.SetConsoleTitleA("etwo | weheartit tool")

urls = []
downloaded = 0 
requests_sent = 0 

def download(url, image_id, file_type):
    global download
    global requests_sent

    try:
        r = req.get(url, stream=True)
        
        if r.status_code == 200:
            r.raw.decode_content = True
            
            with open(f'scraped/{image_id}.{file_type}', 'wb') as f:
                shutil.copyfileobj(r.raw, f)

                with lock:
                    downloaded += 1 
                    print(f"[{Fore.GREEN}scraping{Fore.RESET}] /scraped {downloaded} image")
            else:
                r = req.get(f"https://data.whicdn.com/images/{image_id}/original.gif", stream=True)

                if r.status_code == 200:

                    with open(f'scraped/{image_id}.gif','wb') as f:
                        shutil.copyfileobj(r.raw, f)

                        with lock:
                            downloaded += 1
                            print(f"[{Fore.GREEN}scraping{Fore.RESET}] /scraped {downloaded} images")
            requests_sent += 1 

        except:
            download(url, image_id, file_type)
def scrape(url, page):
    global urls 
    global scraped

    r = req.get(f"{url}?page={page}")
    soup = BeautifulSoup(r.content, 'lxml')
    url = soup.find_all(class_='entry-thumbnail')

    if len(url) == 0:
        return True

    for image in url:
        urls.append(image['src'])

if __name__ == '__main__':
    print(f"[{Fore.GREEN}scraping{Fore.RESET}] etwo | scraper")

    print(f"[{Fore.GREEN}scraping{Fore.RESET}] enter url to scrape:", end="")
    url = input()
    print(f"[{Fore.GREEN}scraping{Fore.RESET}] Pages to scrape?", end="")
    pages = int(input())

    print(f"[{Fore.GREEN}scraping{Fore.RESET}] Scraping...")

    for i in range(pages):
        threading.Thread(target=scrape, args=(url, i+l, )).start()

    t = time.time()
    while threading.active_count() !=1:
        time.sleep(1)

    print(f"\n[{Fore.RED}scraping{Fore.RESET}] Scraped {len(urls)} images in {round(time.time()-t)} seconds!")
    print(f"[{Fore.RED}scraping{Fore.RESET}] Press enter to start downloading: ")
    input()       

    for images in urls: 
        image_id = image.split("/")[4]
        file_type = image.split("/")[5].split(".")[1].split("?")[0]
        url = f"https://data.whicdn.com/images/{image_id}/original.file{file_type}"

        threading.Thread(target=download, args=(url, image_id, file_type,)).start()

        while requests_sent != len(urls):
            time.sleep(1)
        print(f"\n[{Fore.GREEN}scraping{Fore.RESET}] etwo | done!")
        input()
                     
